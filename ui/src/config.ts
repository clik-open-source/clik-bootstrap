export const appConfig = {
  appName: process.env.APP_NAME,
  apiBaseUrl: process.env.API_BASE_URL,
  appUniqueSlug: process.env.APP_UNIQUE_SLUG,
};
