import React from 'react';
import { useValidateSuperAdminSession } from '../../../hooks/auth/useValidateSession';
import { useSuperAdminLogout } from '../../../hooks/auth/useLogout';
import { Page } from '../../../../bootstrap/chakra/components/layouts/Page';
import { AppBar } from './AppBar';
import { Button } from '@chakra-ui/react';
import { Content } from '../../../../bootstrap/chakra/components/layouts/Content';
import { Paper } from '../../../../bootstrap/chakra/components/containers/Paper';
import { SideBar } from '../../../../bootstrap/chakra/components/layouts/sidebar/SideBar';
import { SidebarNavigation } from '../../../../bootstrap/chakra/components/layouts/sidebar/SidebarNavigation';
import { FlexCol } from '../../../../bootstrap/chakra/components/layouts/FlexCol';
import { AppFooter } from './AppFooter';
import { GetLayoutFunction } from '../../../../bootstrap/types';
import { HomeIcon, PasswordIcon, SettingsIcon } from '../../../../bootstrap/chakra/components/icons';


export interface AdminLayoutProps {
  children: React.ReactNode,
}

export function SuperAdminLayout({ children }: AdminLayoutProps) {
  useValidateSuperAdminSession();
  const handleLogout = useSuperAdminLogout();

  return (
    <Page d="flex" flexDir="column">
      <AppBar justify="space-between">
        <Button colorScheme="danger" size="xs" onClick={handleLogout}>Logout</Button>
      </AppBar>
      <Content justifyContent="center" alignItems="stretch" flexGrow={1}>
        <SideBar w={64}>
          <Paper h="100%" pt={4} variant="hoverableRect" _hover={{
            boxShadow: 'rgba(0, 0, 0, 0.1) 10px 0px 15px -3px',
          }}>
            <SidebarNavigation
              items={[
                { key: 'dashboard', label: 'Dashboard', href: '/_admin/dashboard', icon: <HomeIcon/> },
                {
                  key: 'settings', label: 'Settings', icon: <SettingsIcon/>, children: [
                    {
                      key: 'change-pwd',
                      label: 'Change Password',
                      icon: <PasswordIcon/>,
                      href: '/_admin/settings/change-password',
                    },
                  ],
                },
              ]}
            />
          </Paper>
        </SideBar>
        <FlexCol flexGrow={1}>
          {children}
        </FlexCol>
      </Content>
      <AppFooter/>
    </Page>
  );
}

export const getAdminLayout: GetLayoutFunction = (page) => <SuperAdminLayout>{page}</SuperAdminLayout>;
