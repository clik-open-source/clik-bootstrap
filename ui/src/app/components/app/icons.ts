import { chakra } from '@chakra-ui/react';

import { BiMailSend } from 'react-icons/bi';
import { FaRegThumbsUp } from 'react-icons/fa';

export const EmailSentIcon = chakra(BiMailSend);
export const ThumbsUpIcon = chakra(FaRegThumbsUp);

