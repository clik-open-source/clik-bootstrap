import { userSession } from '../../userSession';

const f = userSession.apiFactory;

export const createAdminTokenApi = f.createPostApi('/api/_admin/auth/token');
export const validateAdminSessionApi = f.createGetApi('/api/_admin/auth/token');
export const logoutAdminApi = f.createPostApi('/api/_admin/auth/logout');
export const changeAdminPasswordApi = f.createPostApi('/api/_admin/auth/update-password');
export const forgotAdminPasswordApi = f.createPostApi('/api/_admin/auth/forgot-password');
export const resetAdminPasswordApi = f.createPostApi('/api/_admin/auth/reset-password');
