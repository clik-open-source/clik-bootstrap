import { Service } from '../../bootstrap/service/Service';
import { Router, useRouter } from 'next/router';

class RoutingService extends Service {
  static useService(): RoutingService {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const router = useRouter();
    return super.useService(router);
  }

  constructor(private readonly router: Router) {
    super();
  }

  goto({ url, as, replace = false }: { url: string, as?: string, replace?: boolean}) {
    if (replace) {
      return this.router.replace(url, as);
    } else {
      return this.router.push(url, as);
    }
  }

  async gotoAdminDashboard({ replace = false }: {replace?: boolean} = {}) {
    return this.goto({ url: '/_admin/dashboard', replace });
  }

  async gotoAdminLoginPage({ replace = false }: {replace?: boolean} = {}) {
    return this.goto({ url: '/_admin', replace });
  }
}

export const useRoutingService: () => RoutingService = () => RoutingService.useService();

export { RoutingService };
