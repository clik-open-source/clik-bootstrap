import React from 'react';
import { SuperAdminAuthService, useSuperAdminAuthService } from '../../services/_admin/SuperAdminAuthService';
import { RoutingService, useRoutingService } from '../../services/RoutingService';
import { userSession } from '../../../userSession';

function useValidateSession(onInvalidSession, checkSession, timeoutSeconds = 60) {
  const { user: currentAuthManagerUser } = userSession.useAuthManager();

  const validateSession = React.useCallback(async () => {
    try {
      const user = await checkSession();
      if (currentAuthManagerUser) {
        if (user.id !== currentAuthManagerUser.id) {
          await onInvalidSession();
        }
      }
    } catch (e) {
      await onInvalidSession();
      return {};
    }
  }, [checkSession, onInvalidSession, currentAuthManagerUser]);

  React.useEffect(() => {
    const timer = setInterval(async () => {
      await validateSession();
    }, timeoutSeconds * 1000);

    return () => {
      clearInterval(timer);
    };
  }, [timeoutSeconds, validateSession]);

  React.useEffect(() => {
    if (!currentAuthManagerUser) {
      onInvalidSession().catch(console.error);
    }
  }, [currentAuthManagerUser, onInvalidSession]);

  React.useEffect(() => {
    validateSession().catch(console.error);
  }, [validateSession]);

  return { user: currentAuthManagerUser };
}


export function useValidateSuperAdminSession(timeoutSeconds = 60) {
  const authService: SuperAdminAuthService = useSuperAdminAuthService();
  const routingService: RoutingService = useRoutingService();

  const handleInvalidSession = React.useCallback(async () => {
    await authService.logout();
    await routingService.gotoAdminLoginPage({ replace: true });
  }, [routingService, authService]);

  return useValidateSession(handleInvalidSession, authService.validateAdminSession, timeoutSeconds);
}

