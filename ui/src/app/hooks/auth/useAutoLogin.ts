import React from 'react';
import { userSession } from '../../../userSession';
import { RoutingService } from '../../services/RoutingService';

export interface UseAutoLoginProps {
  onSessionExists: (object) => any;
}

function useAutoLogin({ onSessionExists }: UseAutoLoginProps) {
  const { user } = userSession.useAuthManager();

  if (!!user) {
    onSessionExists(user);
  }
}


export function useAutoSuperAdminLogin() {
  const routingService: RoutingService = RoutingService.useService();

  const handleLoggedIn = React.useCallback(async () => {
    await routingService.gotoAdminDashboard({ replace: true });
  }, [routingService]);

  useAutoLogin({ onSessionExists: handleLoggedIn });
}
