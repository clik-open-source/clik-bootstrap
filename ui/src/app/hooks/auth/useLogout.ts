import { SuperAdminAuthService } from '../../services/_admin/SuperAdminAuthService';
import React from 'react';
import { RoutingService } from '../../services/RoutingService';
import { noopFunc } from '../../../bootstrap/utils/noop';

export type OnLogout = () => any;

export function useLogout(onLogout: OnLogout) {
  const authService: SuperAdminAuthService = SuperAdminAuthService.useService();

  return React.useCallback(async () => {
    await authService.logout();
    await onLogout();
  }, [authService, onLogout]);
}

export function useSuperAdminLogout(onLogout: OnLogout = noopFunc) {
  const routingService: RoutingService = RoutingService.useService();

  const handleLogout = React.useCallback(async () => {
    await routingService.gotoAdminLoginPage();
    await onLogout();
  }, [onLogout, routingService]);

  return useLogout(handleLogout);
}
