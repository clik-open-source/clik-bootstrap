/*
import { useMutation, useQuery, useQueryClient } from 'react-query';


export function useSomeQueries() {
  const someService = useSomeService();
  const queryClient = useQueryClient();

  const useListSomeQuery = () => useQuery('some', someService.list);

  const useGetSomeQuery = (someId) => useQuery(
    ['some', someId],
    () => someService.getSome(someId),
  );

  const createSomeQuery = useMutation(someService.createSome, {
    onSuccess: () => queryClient.invalidateQueries('some'),
  });

  const updateSomeQuery = useMutation(someService.updateSome, {
    onSuccess: () => queryClient.invalidateQueries('some'),
  });

  const removeSomeQuery = useMutation(someService.removeSome, {
    onSuccess: () => queryClient.invalidateQueries('some'),
  });

  return {
    useListSomeQuery,
    useGetSomeQuery,
    useGetSomeBySlugQuery,
    createSomeQuery,
    updateSomeQuery,
    removeSomeQuery,
  };
}
*/
export {};