export type EnumOrKey = Enum | string;

export abstract class Enum<KT extends string = string> {
  public readonly key: KT;
  public readonly label: string;
  private static enums = {};

  static get(key: EnumOrKey) {
    if (this === Enum) {
      throw new Error('You need to call `get` on the appropriate Enum class');
    }

    if (key instanceof Enum) {
      return key;
    }
    return this.enums[key];
  }

  private static add(e: Enum) {
    this.enums[e.key] = e;
  }

  protected constructor(key: KT, label?: string) {
    this.key = key;
    this.label = label || key;
    Enum.add(this);
  }

  toString() {
    return this.key;
  }
}
