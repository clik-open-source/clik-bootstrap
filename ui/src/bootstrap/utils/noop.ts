export const noopFunc = () => undefined;
export const identity = (x) => x;
export const noopArray = [];
