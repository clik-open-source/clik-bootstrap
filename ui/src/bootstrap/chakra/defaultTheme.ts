import { extendTheme, withDefaultColorScheme } from '@chakra-ui/react';
import { theme as defaultChakraTheme } from '@chakra-ui/react';


export const theme = extendTheme(
  withDefaultColorScheme({
    colorScheme: 'blue',
  }),
  {
    styles: {
      global: {
        '*': {
          transition: '0.1s',
        },
      },
    },
  },
  {
    colors: {
      primary: defaultChakraTheme.colors.blue,
      secondary: defaultChakraTheme.colors.teal,
      tertiary: defaultChakraTheme.colors.gray,
      success: defaultChakraTheme.colors.green,
      danger: defaultChakraTheme.colors.red,
      warning: defaultChakraTheme.colors.orange,
    },
    components: {
      Button: {
        baseStyle: {
          _focus: {
            outline: 'none !important',
            boxShadow: 'none !important',
          },
        },
      },
      IconButton: {
        baseStyle: {
          _focus: {
            outline: 'none !important',
            boxShadow: 'none !important',
          },
        },
      },
      CloseButton: {
        baseStyle: {
          _focus: {
            outline: 'none !important',
            boxShadow: 'none !important',
          },
        },
      },
      Input: {
        baseStyle: {
          field: {
            _readOnly: {
              color: 'gray.400',
            },
          },
        },
      },
      Paper: {
        baseStyle: {
          border: 'none',
          _hover: {
            boxShadow: 'sm',
          },
          minH: 0,
          overflow: 'auto',
        },

        variants: {
          hoverable: {
            border: '1px',
            borderColor: 'gray.300',
            rounded: 'md',
            transition: '0.2s',
            _hover: {
              // boxShadow: 'xl',
              boxShadow: 'rgba(0, 0, 0, 0.1) 0px 0px 15px 2px',
            },
          },

          hoverableRect: {
            border: '1px',
            borderColor: 'gray.300',
            rounded: 0,
            transition: '0.2s',
            _hover: {
              boxShadow: 'xl',
            },
          },
        },
      },
    },
  },
);


