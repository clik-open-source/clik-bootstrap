import React, { HTMLAttributeAnchorTarget } from 'react';
import { Button, ButtonProps } from '@chakra-ui/react';
import { Anchor } from './Anchor';

export interface LinkButtonProps extends ButtonProps {
  href: string,
  showUrlAs?: string,
  target?: HTMLAttributeAnchorTarget,
  rel?: HTMLAnchorElement['rel'],
}

export function LinkButton({ href, showUrlAs, target, rel, variant = 'link', ...rest }: LinkButtonProps) {
  return (
    <Anchor href={href} showUrlAs={showUrlAs} target={target} rel={rel} _hover={{
      textDecoration: variant === 'link' ? 'underline' : 'none',
    }}>
      <Button variant={variant} {...rest} />
    </Anchor>
  );
}
