import { DateTime, DateTimeFormatOptions } from 'luxon';
import React from 'react';
import { BodyS } from '../typography';

export interface DateCellRendererProps {
  value: string,
  format?: DateTimeFormatOptions,
}

export function DateCellRenderer({ value, format }: DateCellRendererProps) {
  return (
    <BodyS>{DateTime.fromISO(value).toLocaleString(format)}</BodyS>
  );
}

export interface ShortDateCellRendererProps {
  value: string,
}

export function ShortDateCellRenderer({ value }: ShortDateCellRendererProps) {
  return <DateCellRenderer value={value} format={DateTime.DATETIME_SHORT} />;
}
