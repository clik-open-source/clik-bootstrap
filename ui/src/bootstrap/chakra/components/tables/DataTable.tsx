import { Button, Table, Thead, Tr, Th, Td, Tbody, Flex, Box } from '@chakra-ui/react';
import React from 'react';
import { useTable, useSortBy, useFilters, Column, UseFiltersColumnOptions, UseSortByColumnOptions } from 'react-table';
import { Paper, PaperProps } from '../containers/Paper';
import { RefreshIcon } from '../icons';
import { HeadingS } from '../typography';


export interface DataTableColumn {
  Filter: any;
}

export type DTColumn = UseFiltersColumnOptions<{}> & UseSortByColumnOptions<{}> & Column;

export interface DataTableProps extends PaperProps {
  title?: string,
  columns?: DTColumn[],
  data?: {}[],
  onRefresh?: () => void,
  loading?: boolean,
  condensed?: boolean,
  rightControls?: React.ReactNode,
}

export function DataTable(
  {
    title = '', className = '',
    columns = [], data = [],
    onRefresh, loading = false,
    condensed = false,
    rightControls = null,
    ...rest
  }: DataTableProps) {
  const tableInstance = useTable({ columns, data }, useFilters, useSortBy);
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
  } = tableInstance;

  // @ts-ignore
  return (
    <Paper variant="hoverable" d="flex" flexDir="column" p={4} {...rest}>
      <Flex justify="space-between" align="center">
        <Flex align="center" mb={4}>
          {title && <HeadingS color="primary.500" mr={4}>{title}</HeadingS>}
          {onRefresh && (
            <Button
              size="xs" onClick={onRefresh} disabled={loading}
              leftIcon={<RefreshIcon size={16}/>} colorScheme="secondary"
            >
              Refresh
            </Button>
          )}
        </Flex>
        {rightControls}
      </Flex>

      <Box minH={0} overflow="auto">
        <Table {...getTableProps()} variant="striped" size="sm" colorScheme="gray">
          <Thead mb={8}>
            {// Loop over the header rows
              headerGroups.map((headerGroup) => (
                // Apply the header row props
                // eslint-disable-next-line react/jsx-key
                <Tr {...headerGroup.getHeaderGroupProps()}>
                  {// Loop over the headers in each row
                    headerGroup.headers.map((column) => (
                      // Apply the header cell props
                      // @ts-ignore
                      // eslint-disable-next-line react/jsx-key
                      <Th {...column.getHeaderProps(column.getSortByToggleProps())} verticalAlign="top">
                        {// Render the header
                          column.render('Header')
                        }
                        <span>
                          {/* @ts-ignore */}
                          {column.isSorted ? (column.isSortedDesc ? ' ↓' : ' ↑') : ' ' /* ↕ */}
                        </span>
                        {/* @ts-ignore */}
                        {column.canFilter ? <Box mt={2}>{column.render('Filter')}</Box> : null}
                      </Th>
                    ))}
                </Tr>
              ))}
          </Thead>
          {/* Apply the table body props */}
          <Tbody {...getTableBodyProps()}>
            {// Loop over the table rows
              rows.map((row) => {
                // Prepare the row for display
                prepareRow(row);
                return (
                  // Apply the row props
                  // eslint-disable-next-line react/jsx-key
                  <Tr {...row.getRowProps()}>
                    {// Loop over the rows cells
                      row.cells.map((cell) => {
                        // Apply the cell props
                        return (
                          // eslint-disable-next-line react/jsx-key
                          <Td {...cell.getCellProps()} className="align-middle">
                            {// Render the cell contents
                              cell.render('Cell')}
                          </Td>
                        );
                      })}
                  </Tr>
                );
              })}
          </Tbody>
        </Table>
      </Box>
    </Paper>
  );
}
