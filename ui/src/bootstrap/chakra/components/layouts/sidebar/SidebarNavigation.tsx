import React from 'react';
import { NavigationItem, SidebarNavigationItem } from './SidebarNavigationItem';
import { Divider } from '@chakra-ui/react';
import { FlexCol, FlexColProps } from '../FlexCol';

export interface SidebarNavigationProps extends FlexColProps {
  items: NavigationItem[],
}

export function SidebarNavigation({ items, ...rest }: SidebarNavigationProps) {
  return (
    <>
      <FlexCol align="stretch" {...rest}>
        {items.map((item) => (
          <SidebarNavigationItem key={item.key} item={item} level={1}/>
        ))}
      </FlexCol>
      <Divider/>
    </>
  );
}
