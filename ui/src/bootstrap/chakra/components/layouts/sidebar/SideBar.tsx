import React from 'react';
import { Box, BoxProps } from '@chakra-ui/react';

export interface SideBarProps extends BoxProps {
}

function SideBar({ ...rest }: SideBarProps) {
  return (
    <Box h="100%" alignSelf="stretch" flexShrink={0} {...rest} />
  );
}

export { SideBar };
