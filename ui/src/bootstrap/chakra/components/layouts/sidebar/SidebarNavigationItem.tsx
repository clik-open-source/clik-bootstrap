import React, { ReactElement } from 'react';
import Link from 'next/link';
import { Box, Button, chakra, Flex, Link as CLink, Collapse } from '@chakra-ui/react';
import { BodyM, BodyS } from '../../typography';
import { useToggle } from '../../../../hooks/utils/useToggle';
import { CollapseIcon, ExpandIcon } from '../../icons';
import { FlexCol } from '../FlexCol';
import { LinkButton } from '../../core/LinkButton';

export interface NavigationItem {
  key: string,
  target?: string,
  href?: string,
  icon?: ReactElement,
  as?: string,
  label: string,
  children?: NavigationItem[],
  hidden?: boolean,
}

export interface ChildItemProps {
  item: NavigationItem,
  level: number,
}

function ChildItem({ item, level }: ChildItemProps) {
  return (
    <LinkButton
      leftIcon={item?.icon}
      size="sm" href={item.href} pl={level * 4}
      variant="ghost" w="100%" justifyContent="start"
    >
      {item.label}
    </LinkButton>
  );
}

export interface ParentItemProps {
  level: number,
  item: NavigationItem,
}

function ParentItem({ level, item }: ParentItemProps) {
  const [expanded, toggleExpanded] = useToggle(false);

  return (
    <FlexCol overflow="hidden">
      <Button
        pl={level * 4} size="sm" variant="ghost" onClick={toggleExpanded} justifyContent="start" leftIcon={item?.icon}
      >
        {item.label} {expanded ? <CollapseIcon ml={1}/> : <ExpandIcon ml={1}/>}
      </Button>
      <Collapse in={expanded} animateOpacity>
        <div>
          {item.children.map((childItem) => (
            <SidebarNavigationItem key={childItem.key} item={childItem} level={level + 1}/>
          ))}
        </div>
      </Collapse>
    </FlexCol>
  );
}

export interface SidebarNavigationItemProps {
  item: NavigationItem,
  level: number,
}

export function SidebarNavigationItem({ item, level }: SidebarNavigationItemProps) {
  if (item.hidden) {
    return null;
  }

  if (item.href) {
    return (
      <ChildItem item={item} level={level}/>
    );
  }

  return (
    <ParentItem item={item} level={level}/>
  );
}
