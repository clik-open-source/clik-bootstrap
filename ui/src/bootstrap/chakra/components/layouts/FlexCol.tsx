import React from 'react';
import { Flex, FlexProps } from '@chakra-ui/react';

export interface FlexColProps extends FlexProps {
}

export function FlexCol(props: FlexColProps) {
  return <Flex direction="column" overflow="auto" minH={0} {...props} />;
}
