import React from 'react';
import { Box, BoxProps } from '@chakra-ui/react';

export interface PageProps extends BoxProps {

}

export function Page(props: PageProps) {
  return (
    <Box w="100vw" h="100vh" {...props} />
  );
}
