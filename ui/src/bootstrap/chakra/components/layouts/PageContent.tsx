import React from 'react';
import { HeadingM } from '../typography';
import { Divider, Flex } from '@chakra-ui/react';
import { FlexCol, FlexColProps } from './FlexCol';

export interface PageContentProps extends FlexColProps {
  pageTitle: React.ReactNode,
  children?: React.ReactNode,
  mainActionButton?: React.ReactNode,
}

export function PageContent({ pageTitle, children, mainActionButton = null, ...rest }: PageContentProps) {
  return (
    <FlexCol p={4} align="stretch" w="100%" {...rest}>
      <Flex justify="space-between" align="center">
        <HeadingM>{pageTitle}</HeadingM>
        {mainActionButton}
      </Flex>
      <Divider my={4} mx={-4} w="auto"/>
      {children}
    </FlexCol>
  );
}
