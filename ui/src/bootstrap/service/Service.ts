import React from 'react';

export class Service {
  private static instance;

  /**
   * Hook to use a service
   * @return {*}
   */
  static useService(...args) {
    // eslint-disable-next-line react-hooks/rules-of-hooks,react-hooks/exhaustive-deps
    return React.useMemo(() => this.getService(...args), []);
  }

  static getService(...args) {
    if (!this.instance) {
      this.instance = new this(...args);
    }
    return this.instance;
  }

  // noinspection JSUnusedLocalSymbols
  constructor(...rest) {
  }
}

