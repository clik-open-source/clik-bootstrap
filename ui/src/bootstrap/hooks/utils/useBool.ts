import { useIsVisible } from './useIsVisible';

export const useBool = useIsVisible;
