import ApiClient from './ApiClient';
import { Method, ResponseType } from 'axios';

export type ApiFactoryConfig = {
  apiClient: ApiClient,
}

export type UrlOrBuilder = string | ((any) => string);
export type ApiFactoryOptions = {
  responseType?: ResponseType,
  inputTransformer?: (any) => any,
  makeHeaders?: (any) => Record<string, string>,
}
export type ApiCallOptions = {
  urlParams?: any,
  data?: any,
  queryParams?: any,
}

class ApiFactory {
  private readonly apiClient: ApiClient;

  constructor({ apiClient }: ApiFactoryConfig) {
    this.apiClient = apiClient;
  }

  apiFactoryBuilder =
    (method: Method) =>
      (
        urlOrBuilder: UrlOrBuilder,
        { responseType = undefined, inputTransformer = null, makeHeaders }: ApiFactoryOptions = {},
      ) =>
        ({ urlParams = {}, data = undefined, queryParams = {} }: ApiCallOptions = {}) =>
          // eslint-disable-next-line no-invalid-this
          this.apiClient.makeRequest({
            method,
            url: typeof urlOrBuilder === 'string' ? urlOrBuilder : urlOrBuilder(urlParams),
            data: inputTransformer ? inputTransformer(data) : data,
            responseType,
            params: queryParams,
            options: {
              headers: makeHeaders?.(urlParams),
            },
          });

  // eslint-disable-next-line no-invalid-this
  createGetApi = this.apiFactoryBuilder('get');
  // eslint-disable-next-line no-invalid-this
  createPostApi = this.apiFactoryBuilder('post');
  // eslint-disable-next-line no-invalid-this
  createPatchApi = this.apiFactoryBuilder('patch');
  // eslint-disable-next-line no-invalid-this
  createPutApi = this.apiFactoryBuilder('put');
  // eslint-disable-next-line no-invalid-this
  createDeleteApi = this.apiFactoryBuilder('delete');
}

export default ApiFactory;
