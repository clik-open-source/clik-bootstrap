import { UserSession } from './bootstrap/session/UserSession';
import { appConfig } from './config';

const userSession = new UserSession(appConfig);

export { userSession };
