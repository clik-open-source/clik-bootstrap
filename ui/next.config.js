require('dotenv-flow').config({
  purge_dotenv: true,
});

module.exports = {
  env: {
    APP_NAME: process.env.APP_NAME,
    API_BASE_URL: process.env.API_BASE_URL,
    APP_UNIQUE_SLUG: process.env.APP_UNIQUE_SLUG,
  },
};
