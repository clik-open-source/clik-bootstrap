import React from 'react';
import { PageComponent } from '../../src/bootstrap/types';
import { getDefaultLayout } from '../../src/app/components/app/layouts/DefaultLayout';
import { SuperAdminLoginForm } from '../../src/app/components/app/forms/SuperAdminLoginForm';
import { useAutoSuperAdminLogin } from '../../src/app/hooks/auth/useAutoLogin';
import { useSuperAdminAuthService } from '../../src/app/services/_admin/SuperAdminAuthService';
import { useRoutingService } from '../../src/app/services/RoutingService';
import { LinkButton } from '../../src/bootstrap/chakra/components/core/LinkButton';
import { FlexCol } from '../../src/bootstrap/chakra/components/layouts/FlexCol';

const AdminHomePage: PageComponent = () => {
  useAutoSuperAdminLogin();
  const authService = useSuperAdminAuthService();
  const routingService = useRoutingService();

  const handleLogin = React.useCallback(async (values) => {
    await authService.loginAdmin(values);
    await routingService.gotoAdminDashboard({ replace: true });
  }, [authService, routingService]);

  return (
    <FlexCol w={96} mt={40} align="center">
      <SuperAdminLoginForm onSubmit={handleLogin} w="100%" mb={10}/>
      <LinkButton href="/">Goto Home Page</LinkButton>
    </FlexCol>
  );
};

AdminHomePage.getLayout = getDefaultLayout;

export default AdminHomePage;
