import React from 'react';
import { AppProps } from 'next/app';
import { Store } from 'redux';
import * as PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import { ChakraProvider } from '@chakra-ui/react';
import nextWithRedux from '../src/bootstrap/hoc/nextWithRedux';
import { QueryClient, QueryClientProvider } from 'react-query';

import '../styles/index.css';

import { PageComponent } from '../src/bootstrap/types';
import { defaultTheme } from '../src/bootstrap/chakra';
import Head from 'next/Head';
import { appConfig } from '../src/config';

const queryClient: QueryClient = new QueryClient();

type ReduxAppProps = AppProps & {
  reduxStore: Store,
  Component: PageComponent,
};

function MainApp({ Component, pageProps, reduxStore }: ReduxAppProps) {
  const getLayout = Component.getLayout || ((page) => page);

  return (
    <>
      <Head>
        <title>{appConfig.appName}</title>
      </Head>
      <ChakraProvider resetCSS theme={defaultTheme}>
        <Provider store={reduxStore}>
          <QueryClientProvider client={queryClient}>
            {getLayout(<Component {...pageProps} />)}
          </QueryClientProvider>
        </Provider>
      </ChakraProvider>
    </>
  );
}

MainApp.propTypes = {
  Component: PropTypes.elementType,
  pageProps: PropTypes.object,
  reduxStore: PropTypes.object,
};

// noinspection JSUnusedGlobalSymbols
export default nextWithRedux(MainApp);
