import React from 'react';
import { PageComponent } from '../src/bootstrap/types';
import { FlexCol } from '../src/bootstrap/chakra/components/layouts/FlexCol';
import { BodyM, HeadingM } from '../src/bootstrap/chakra/components/typography';
import { getDefaultLayout } from '../src/app/components/app/layouts/DefaultLayout';
import { LinkButton } from '../src/bootstrap/chakra/components/core/LinkButton';

function HomeNotImplementedYet() {
  return (
    <FlexCol mt={40} align="center">
      <HeadingM>Nothing Yet Implemented</HeadingM>
      <BodyM mt={20} mb={4}>If you are the admin, you can login here</BodyM>
      <LinkButton href="/_admin" variant="solid">Go to Admin Login</LinkButton>
    </FlexCol>
  );
}

const Home: PageComponent = () => (
  <HomeNotImplementedYet/>
);
Home.getLayout = getDefaultLayout;

export default Home;

