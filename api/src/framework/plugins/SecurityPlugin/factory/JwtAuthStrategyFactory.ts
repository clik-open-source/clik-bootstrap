import { ExtractJwt, Strategy as JwtStrategy } from 'passport-jwt/lib';
import { ISecurityUserService } from '../models/ISecurityUserService';
import { JwtPayload } from '../models/JwtPayload';
import { ISecurityUser } from '../models/ISecurityUser';
import { JwtConfig } from '../models/JwtConfig';
import { AbstractAuthStrategyFactory } from './AbstractAuthStrategyFactory';
import { JwtService } from '../service/JwtService';

const defaultTokenExtractor = ExtractJwt.fromAuthHeaderAsBearerToken();

export type JwtAuthStrategyFactoryOptions = JwtConfig;

export class JwtAuthStrategyFactory extends AbstractAuthStrategyFactory {
  constructor(private readonly jwtService: JwtService) {
    super();
  }

  createStrategy(
    userService: ISecurityUserService,
    jwtOptions: JwtAuthStrategyFactoryOptions,
    tokenExtractor = defaultTokenExtractor,
  ) {
    return new JwtStrategy(
      {
        secretOrKey: jwtOptions.secret,
        jwtFromRequest: tokenExtractor,
        passReqToCallback: true,
        issuer: jwtOptions.issuer,
      },
      async function(req, jwtPayload: JwtPayload, done) {
        const { uid } = jwtPayload;
        try {
          const user: ISecurityUser = await userService.findByUid(uid, null);

          if (!user) {
            return done(null, false);
          }

          const requestToken = defaultTokenExtractor(req);
          const userToken = await userService.findAuthToken(requestToken, user);

          if (!userToken) {
            return done(null, false);
          }

          user.activeAuthToken = userToken;
          done(null, user);
        } catch (error) {
          done(error);
        }
      },
    );
  }

  async createToken(payload: any, options: JwtConfig) {
    return this.jwtService.createToken(payload, options);
  }

  async verifyToken(token: string, options: JwtConfig) {
    return this.jwtService.verifyToken(token, options);
  }
}
