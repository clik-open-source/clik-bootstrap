import { ISecurityUserToken } from './ISecurityUserToken';

export interface ISecurityUser {
  uid: string,
  secret: string,
  tokens: Promise<ISecurityUserToken[]>,
  roles: string[] | any[],
  activeAuthToken?: ISecurityUserToken,
  enabled?: boolean,
}
