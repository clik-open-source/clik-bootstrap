import { FactoryName, FactoryOptions, FilteredServiceLocator } from '../../../core/container';
import { AbstractFileServer } from '../engines/AbstractFileServer';

export class FileServerLocator extends FilteredServiceLocator {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  filter(name: FactoryName, options: FactoryOptions): boolean {
    return typeof name === 'function' && name.prototype instanceof AbstractFileServer;
  }
}
