import { FileServerLocator } from './FileServerLocator';
import { AbstractFileServer } from '../engines/AbstractFileServer';
import { FactoryName } from '../../../core/container';

export class FileServerService {
  constructor(
    private readonly fileServerLocator: FileServerLocator,
    private readonly defaultEngine: string,
  ) {
  }

  async get(engine: FactoryName = null): Promise<AbstractFileServer> {
    let factory = engine ?? this.defaultEngine;
    if (typeof factory === 'string') {
      factory = `fileServer.engine.${factory}`;
    }
    return this.fileServerLocator.resolve2(factory);
  }
}
