import { AbstractFileServer, FileData } from './AbstractFileServer';
import {
  CopyObjectCommand, CopyObjectCommandInput,
  DeleteObjectCommand,
  DeleteObjectCommandInput, GetObjectCommand, GetObjectCommandInput,
  PutObjectCommand,
  PutObjectCommandInput,
  S3Client,
} from '@aws-sdk/client-s3';
import { Readable } from 'stream';

type AWSS3Path = {
  Bucket: string,
  Key: string,
}

export type AWSS3FileServerOptions = {
  region?: string,
  accessKeyId?: string,
  secretAccessKey?: string,
  bucketKeySeparator: string,
}

export type AWSS3FileServerCreateOptions = Omit<PutObjectCommandInput, 'Body' | 'Key' | 'Bucket'>
export type AWSS3FileServerReadOptions = Omit<GetObjectCommandInput, 'Key' | 'Bucket'>
export type AWSS3FileServerDeleteOptions = Omit<DeleteObjectCommandInput, 'Key' | 'Bucket'>
export type AWSS3FileServerCopyOptions = Omit<CopyObjectCommandInput, 'Key' | 'Bucket' | 'CopySource'>
export type AWSS3FileServerMoveOptions = {
  copy?: AWSS3FileServerCopyOptions,
  delete?: AWSS3FileServerDeleteOptions,
}


export class AWSS3FileServer extends AbstractFileServer {
  private client: S3Client;

  constructor(
    private readonly config: AWSS3FileServerOptions,
  ) {
    super();
    this.client = new S3Client({
      region: config.region,
      credentials: {
        accessKeyId: config.accessKeyId,
        secretAccessKey: config.secretAccessKey,
      },
    });
  }

  private getBucketAndKeyFromFilePath(filePath: string): AWSS3Path {
    const slashIndex = filePath.indexOf(this.config.bucketKeySeparator);
    if (slashIndex === -1) {
      throw new Error('Invalid AWS S3 file name');
    }
    return {
      Bucket: filePath.substr(0, slashIndex),
      Key: filePath.substr(slashIndex + 1),
    };
  }

  async copyFile(
    sourceServerFilePath: string,
    destinationServerFilePath: string,
    options: AWSS3FileServerCopyOptions = {},
  ): Promise<any> {
    if (!sourceServerFilePath.startsWith('/')) {
      sourceServerFilePath = '/' + sourceServerFilePath;
    }
    const copyCommand = new CopyObjectCommand({
      ...options,
      CopySource: sourceServerFilePath,
      ...this.getBucketAndKeyFromFilePath(destinationServerFilePath),
    });
    return this.client.send(copyCommand);
  }

  async createFile(
    serverFilePath: string,
    fileData: FileData | Promise<FileData>,
    options: AWSS3FileServerCreateOptions = {},
  ): Promise<any> {
    const putCommand = new PutObjectCommand({
      ...options,
      Body: await fileData,
      ...this.getBucketAndKeyFromFilePath(serverFilePath),
    });
    return this.client.send(putCommand);
  }

  async deleteFile(serverFilePath: string, options: AWSS3FileServerDeleteOptions = {}): Promise<any> {
    const deleteCommand = new DeleteObjectCommand({
      ...options,
      ...this.getBucketAndKeyFromFilePath(serverFilePath),
    });
    return this.client.send(deleteCommand);
  }

  async moveFile(
    sourceServerFilePath: string,
    destinationServerFilePath: string,
    options: AWSS3FileServerMoveOptions = {},
  ): Promise<any> {
    await this.copyFile(sourceServerFilePath, destinationServerFilePath, options.copy || {});
    return this.deleteFile(sourceServerFilePath, options.delete || {});
  }

  async readFile(serverFilePath: string, options: AWSS3FileServerReadOptions = {}): Promise<Readable> {
    const getCommand = new GetObjectCommand({
      ...options,
      ...this.getBucketAndKeyFromFilePath(serverFilePath),
    });
    const response = await this.client.send(getCommand);
    return <Readable>response.Body;
  }
}
