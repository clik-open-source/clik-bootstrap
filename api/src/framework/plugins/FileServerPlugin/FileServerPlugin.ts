import { AbstractPlugin } from '../../core/plugins/AbstractPlugin';
import { ServiceContainer } from '../../core/container';
import { LocalFileServer } from './engines/LocalFileServer';
import { FileServerLocator } from './service/FileServerLocator';
import { FileServerService } from './service/FileServerService';
import { AWSS3FileServer, AWSS3FileServerOptions } from './engines/AWSS3FileServer';

export type FileServerPluginOptions = {
  defaultEngine: string,
  local?: {
    rootPath: string,
  },
  // eslint-disable-next-line camelcase
  aws_s3?: AWSS3FileServerOptions
}

export class FileServerPlugin extends AbstractPlugin {
  getDefaultNamespace() {
    return 'fileServer';
  }

  getDefaultConfig(): FileServerPluginOptions {
    return {
      defaultEngine: 'local',
      local: {
        rootPath: './localFileServerData',
      },
      aws_s3: {
        bucketKeySeparator: '/',
      },
    };
  }

  registerServices(serviceContainer: ServiceContainer, config: FileServerPluginOptions) {
    const { decorators: { injectable } } = serviceContainer;

    injectable({
      getDependenciesList: () => [
        config.local.rootPath,
      ],
      alias: `${this._namespace}.engine.local`,
    })(LocalFileServer);

    injectable({
      getDependenciesList: () => [
        config.aws_s3,
      ],
      alias: `${this._namespace}.engine.aws_s3`,
    })(AWSS3FileServer);

    injectable()(FileServerLocator);

    injectable({
      getDependenciesList: async (resolve) => [
        await resolve(FileServerLocator),
        config.defaultEngine,
      ],
    })(FileServerService);
  }
}
