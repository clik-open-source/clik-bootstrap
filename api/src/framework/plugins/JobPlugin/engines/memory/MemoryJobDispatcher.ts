import { AbstractJobDispatcher } from '../../models/AbstractJobDispatcher';
import { Job } from '../../models/Job';
import { MemoryJobHandler } from './MemoryJobHandler';

export class MemoryJobDispatcher extends AbstractJobDispatcher {
  constructor(private readonly jobHandler: MemoryJobHandler) {
    super();
  }

  async dispatch(jobName: string, jobData: any, options: any) {
    const job = new Job(jobName, jobData, options);
    return this.jobHandler.handle(job);
  }
}
