# Framework

Contains framework code

- core - contains framework core:
  - Container
  - Autoloader
  - Kernel
  - Plugins manager
- plugins - contains plugins
  - AppConfigPlugin
  - CommandPlugin
  - EventPlugin
  - FileServerPlugin
  - JobPlugin
  - LoggingPlugin
  - MailerPlugin
  - SecurityPlugin
  - SerializerPlugin
  - TemplatePlugin
  - TypeORMPlugin
  - WebPlugin
