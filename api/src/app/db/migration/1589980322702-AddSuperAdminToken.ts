import { MigrationInterface, QueryRunner } from 'typeorm';

/* eslint-disable max-len */

// noinspection JSUnusedGlobalSymbols
export class AddSuperAdminToken1589980322702 implements MigrationInterface {
  name = 'AddSuperAdminToken1589980322702';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query('CREATE TABLE `super_admin_token` (`id` varchar(36) NOT NULL, `token` varchar(1024) NOT NULL, `tokenType` varchar(255) NOT NULL, `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `userId` varchar(36) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB', undefined);
    await queryRunner.query('ALTER TABLE `super_admin_token` ADD CONSTRAINT `FK_27a854677e54a0857b299fe18f2` FOREIGN KEY (`userId`) REFERENCES `super_admin`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION', undefined);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query('ALTER TABLE `super_admin_token` DROP FOREIGN KEY `FK_27a854677e54a0857b299fe18f2`', undefined);
    await queryRunner.query('DROP TABLE `super_admin_token`', undefined);
  }
}
