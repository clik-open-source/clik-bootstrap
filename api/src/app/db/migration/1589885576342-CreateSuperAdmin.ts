import { MigrationInterface, QueryRunner } from 'typeorm';

/* eslint-disable max-len */
// noinspection JSUnusedGlobalSymbols
export class CreateSuperAdmin1589885576342 implements MigrationInterface {
  name = 'CreateSuperAdmin1589885576342';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query('CREATE TABLE `super_admin` (`id` varchar(36) NOT NULL, `name` varchar(255) NOT NULL, `email` varchar(255) NOT NULL, `password` varchar(2048) NOT NULL, `roles` json NOT NULL, `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), UNIQUE INDEX `IDX_de87485f6489f5d0995f584195` (`email`), PRIMARY KEY (`id`)) ENGINE=InnoDB', undefined);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query('DROP INDEX `IDX_de87485f6489f5d0995f584195` ON `super_admin`', undefined);
    await queryRunner.query('DROP TABLE `super_admin`', undefined);
  }
}
