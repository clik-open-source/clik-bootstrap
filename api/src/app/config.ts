import * as def from 'dotenv-flow';
import * as winston from 'winston';

def.config({
  purge_dotenv: true,
});

const config = {
  app: {
    ui_app: {
      base_url: process.env.UI_APP_BASE_URL,
    },
  },

  logging: {
    transports: {
      file: {
        factory: 'logging.transport.factory.file',
        options: {
          filename: 'logs.log',
          dirname: 'logs',
        },
      },
      console: {
        factory: 'logging.transport.factory.console',
        options: {},
      },
    },
    loggers: {
      default: {
        level: 'debug',
        getFormat: () => winston.format.combine(
          winston.format.timestamp(),
          winston.format.json(),
        ),
        transports: ['file', 'console'],
      },
    },
  },

  web: {
    server: {
      port: process.env.WEB_SERVER_PORT,
    },
  },

  security: {
    firewalls: {
      superAdminJwt: {
        factory: 'security.auth.JwtAuthStrategyFactory',
        userProvider: 'app.service.SuperAdminService',
        options: {
          issuer: process.env.SECURITY_DEFAULT_JWT_ISSUER,
          secret: process.env.SECURITY_DEFAULT_JWT_SECRET,
          expiresIn: process.env.SECURITY_DEFAULT_JWT_EXPIRY,
        },
      },
      superAdminLocal: {
        factory: 'security.auth.LocalAuthStrategyFactory',
        userProvider: 'app.service.SuperAdminService',
      },
    },
  },

  typeorm: {
    connections: [{
      name: 'default',
      type: 'mysql',
      host: process.env.TYPEORM_DB_HOST,
      port: process.env.TYPEORM_DB_PORT,
      username: process.env.TYPEORM_DB_USER,
      password: process.env.TYPEORM_DB_PASSWORD,
      database: process.env.TYPEORM_DB_DATABASE,
      synchronize: false,
      logging: false,
      entities: [process.env.TYPEORM_DB_ENTITIES_PATH],
      migrations: [process.env.TYPEORM_DB_MIGRATIONS_PATH],
      subscribers: [process.env.TYPEORM_DB_SUBSCRIBERS_PATH],
      cli: {
        entitiesDir: 'src/app/db/entity',
        migrationsDir: 'src/app/db/migration',
        subscribersDir: 'src/app/db/subscriber',
      },
    }],
  },

  jobs: {
    defaultEngine: 'jobs.engine.memory',
    handlers: {
      mail: ['job.mail.handler'],
    },
  },

  mailer: {
    defaultTransport: process.env.MAILER_DEFAULT_TRANSPORT,
    defaultSender: process.env.MAILER_DEFAULT_SENDER,
    transports: {
      stream: {
        newline: 'unix',
      },
      json: {
        skipEncoding: true,
      },
      mailgun: {
        auth: {
          api_key: process.env.MAILER_MAILGUN_KEY,
          domain: process.env.MAILER_MAILGUN_DOMAIN,
        },
        proxy: false,
      },
    },
  },

  template: {
    defaultEngine: 'template.engine.nunjucks',
    nunjucks: {
      templatesPath: 'src/app/views',
    },
  },

  fileServer: {
    defaultEngine: 'local',
    local: {
      rootPath: './temp/localFileServerData',
    },
  },
};

export default config;

export type AppConfigType = typeof config;
