import { inject, injectable } from '../boot';
import { IUser } from '../../bootstrap/models/IUser';
import { ISecurityUserToken } from '../../framework/plugins/SecurityPlugin/models/ISecurityUserToken';
import { UiAppUrlHelper } from './UiAppUrlHelper';
import * as Mail from 'nodemailer/lib/mailer';
import { TemplateService } from '../../framework/plugins/TemplatePlugin/service/TemplateService';
import { SuperAdmin } from '../db/entity/SuperAdmin';


@injectable()
export class MailsManager {
  constructor(
    @inject(UiAppUrlHelper) private readonly uiAppUrlHelper: UiAppUrlHelper,
    @inject(TemplateService) private readonly templates: TemplateService,
  ) {
  }

  async resetPasswordRequestedMailData(user: IUser, resetToken: ISecurityUserToken): Promise<Mail.Options> {
    const isAdmin = user instanceof SuperAdmin;
    const mailData = {
      user: {
        name: user.name,
        email: user.email,
      },
      resetLink: isAdmin ? this.uiAppUrlHelper.resetSuperAdminPasswordUrl(user.email, resetToken.token) : '',
    };

    return {
      to: user.email,
      subject: 'mailer.auth.resetPasswordRequested.subject',
      text: JSON.stringify({
        mail: 'Reset Password Requested',
        mailData,
      }, null, 2),
      html: await this.templates.render('mails/reset-password-requested.njk.html', mailData),
    };
  }
}
