import { injectable } from '../boot';
import { config } from '../../framework/plugins/AppConfigPlugin/AppConfigPlugin';

@injectable()
export class UiAppUrlHelper {
  constructor(@config('ui_app.base_url') private readonly appBaseUrl: string) {
  }

  resetSuperAdminPasswordUrl(email: string, resetToken: string) {
    const payload = Buffer.from(JSON.stringify({
      email,
      token: resetToken,
    })).toString('base64');

    return `${this.appBaseUrl}/_admin/auth/reset-password?t=${payload}`;
  }
}
