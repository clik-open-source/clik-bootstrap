Bootstrap

Contains services and utilities useful for the bootstrap project. These don't form part of the framework code but 
the *project* itself. However, it would be useful to have these outside the *app* scope so that they are easy to 
upgrade and back-port.

